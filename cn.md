# innov商城 入侵cn适应API文档

[TOC]

## 用户前端增加接口
### 简介
> 目前地址暂定为http://onmyway.ml:1001
> 请求方式为post，此接口基本需要登录，在Authentication请求头里增加登录时返回的token

### 1. 获取收货地址
> url: http://onmyway.ml:1001/authapi/getAddressCnList

### 2. 根据编号获取指定收货地址
> url: http://onmyway.ml:1001/authapi/getAddressCnById
|参数名|描述|类型|是否必填|
|:--:|:--:|:--:|:--:|
|id|编号|string|是|

### 3. 添加收货地址
> url: http://onmyway.ml:1001/authapi/saveAddressCn
|参数名|描述|类型|是否必填|
|:--:|:--:|:--:|:--:|
|addrId|填表示编辑，不填表示新增|string|否|
|name|收件人姓名|string|是|
|province|省|string|是|
|city|市|string|是|
|county|县|string|是|
|address|详细地址|string|是|
|code|邮政编码|string|否|
|mobile|手机号|string|是|
|isDefault|是否默认收货地址|bool|是|

### 4. 设置收货地址（删除/设为默认
> url: http://onmyway.ml:1001/authapi/setAddresscn
|参数名|描述|类型|是否必填|
|:--:|:--:|:--:|:--:|
|id|编号|string|是|
|action|操作，set表示设为默认，del表示删除该收货地址|string|是|


### 5. 用户信息
> url: http://onmyway.ml:1001/authapi/userInfo
> 增加deliveryaddressCn字段，返回收货地址列表

### 6. 下单
> url: http://onmyway.ml:1001/authapi/userInfo
> 现在不需要信用卡信息，而且不会在下单的时候支付

### 7. 支付
> url: http://onmyway.ml:1001/authapi/bindPayId
> 会返回charge对象，前端再试用charge对象进行支付
|参数名|描述|类型|是否必填|
|:--:|:--:|:--:|:--:|
|orderNo|订单编号|string|是|
|payway|支付方式，6表示设为支付宝，7表示银联|int|是|

