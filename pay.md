## 信用卡的区分
前6位数字（包含第一位数字）是发卡机构代码（Issuer Identification Number, IIN），用于标识具体的卡片发行机构。
- Visa（维萨）：4xxxxx 16位（以前有极少数的卡为13位，现已停止发行了）
- Mastercard（万事达）：51xxxx – 55xxxx 16位

>信用卡卡号校验方法：
>1. 从右边开始，将偶数位的数字乘以 2；
>2. 将得到的数字和刚才剩余的（奇次位）的所有数字相加，如果遇到乘以 2 后得到的数字是 2 位数的，则将其个位和十位数相加；
>3. 如果得到的数字之和是 10 的倍数，则号码为真，否则就是假的信用卡号了。
>![](http://i.imgur.com/1MsSmTp.png)

## 第三方支付平台
### PayPal
支付流程：客户提交订单-》跳转到Paypal-》Paypal支付确认-》完成付款跳转返回
>Paypal支付流程演示：https://www.paypal-biz.com/product/demo/product/product_01/cn/index01.html

### SQID
支付流程：客户提交订单-》填写信用卡信息（持卡人、卡号、有效期、CVV）-》后台发送付款api-》api返回支付结果
