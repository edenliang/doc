## 简介
此文档为innovfoods_for_cn 的文档

- [有更改的文档](https://gitlab.com/edenliang/doc/blob/master/cn.md)

## 旧开发文档
- [简介Api](https://gitlab.com/edenliang/doc/blob/master/main.md)
- [前台Api](https://gitlab.com/edenliang/doc/blob/master/前台.md)
- [供应商后台](https://gitlab.com/edenliang/doc/blob/master/供应商后台.md)
- [后台Api](https://gitlab.com/edenliang/doc/api/master/后台.md)
- PostMan：https://www.getpostman.com/collections/81b9c096c0fb2dc13e65
- [支付文档](https://gitlab.com/edenliang/doc/blob/master/pay.md)
- [SQID 快速付款 API(SRP)](https://gitlab.com/edenliang/blob/master/doc/SRP.md)
- [发布](https://gitlab.com/edenliang/doc/blob/master/build.md)