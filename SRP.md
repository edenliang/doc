## SQID 快速付款 API(SRP)
>本文件为指导负责系统集成与SQID支付网关的程序员。我们假定读者熟悉POST、JSON、HTTP和SSL。并且了解基本的API通信工作。

## 概述
这份文件是SQID的知识产权。非法复制和传播本文件以任何方式和任何媒体都是严格禁止的。注意，本文档的任何当前或将来的开发人员将与SQID披露和保密协议的约束。
这一节必须复制这个文件的所有副本。在任何情况下不得从文档中移除。
这份出版物的版权。 除了所述的目的和条件根据现行的著作权法，它的任何部分，未经SQID事先书面许可，不得以任何形式或任何手段复制、存储或传播。
##请求格式
所有API请求被发送到SRP平台使用POST(Json)方法，必须通过HTTPS协议执行。
请求必须发送到
- https://api.sqidpay.com/post (live)
- https://api.staging.sqidpay.com/post  (testing). 

## API
### processPayment
支付，用于执行一个购买请求。

|参数|描述|
|:---|:----|
|methodName|方法名|
|merchantCode|商户名称,这是SQID分配给你一个唯一的代码|
|apikey|这是SQID分配给你一个唯一的代码|
|amount|总金额,数额应的格式设置为两位小数。没有两位小数或小于1.00美元的金额将被拒绝。|
|Currency |目前仅支持澳元（AUD）|
|referenceID|这是系统交易的唯一ID(交易订单号)。有效长度：最多36个字符|
|customerName|顾客名，此字段只能包含字母数字字符，而且必须有至少一个空格。有效长度：最多255个字符|
|customerHouseStreet |此字段应包含客户的街道名称和编号。有效长度：最多255个字符|
|customerCity |此字段应包含客户的城市名。这是一个自由文本字段。 有效长度：最多255个字符|
|customerState |此字段应包含客户的洲。这是一个自由文本字段。 有效长度：最多255个字符|
|customerCountry | ISO 3166-1 alpha-3 国家代码|
|customerPostCode |此字段应包含客户的邮编。有效长度：4-10个字符|
|customerMobile |客户的手机号码，有效字符是+，空格和0-9|
|customerEmail |客户的电子邮件地址，有效长度：最多255个字符|
|customerIP |客户的IP地址，有效长度：最多45个字符|
|cardNumber |卡号不应该有任何空格或其他分隔符。有效长度：13-16位|
|cardExpiry|应该出现在MMYY格式。 如果月低于10，这应该是零填充。有效长度：4位数字|
|cardName|这应该至少有一个空格字符。空间完全停止允许，只有数字和字母组成。有效长度：最多255个字符|
|cardCSC|也称为CVV或CVN，三位数字|
|hashValue |这是通过执行生成一个MD5具体参数。（组成：PassPhrase + Amount + APIKey）有效长度：32个字符|
|customField1|这是一个自定义参数文本字段,有效长度：最多255个字符(非必需字段)|
|customField2 |这是一个自定义参数文本字段,有效长度：最多255个字符(非必需字段)|
|customField3|这是一个自定义参数文本字段,有效长度：最多255个字符(非必需字段)|
>请注意，应传递的所有参数必须有一个值。，即使他们没有任何值。在这种情况下，值应为空字符串。

```
"methodName": "processPayment", //方法名
"merchantCode": "MC", //商户名称,这是被SQID分配给你一个唯一的代码
"apiKey": "ABCDEBC647FD7CA4CDD65DF74B18D39",//apikey,这是被SQID分配给你一个唯一的代码 
"amount": "11.50", //总金额,数额应的格式设置为两位小数。
"currency": "AUD", //币种
"referenceID": "RI1", //编号
"customerName": "Bob Smith",// 顾客姓名
"customerHouseStreet": "10 Ipswich Rd",// 街道
"customerSuburb": "Annerley", //城区
"customerCity": "Brisbane", //城市
"customerState": "QLD", //洲
"customerCountry": "AUS", //国家
"customerPostCode": "4103", //邮编
"customerMobile": "0400000000", //电话
"customerEmail": "person@company.com",//Email 
"customerIP": "127.0.0.1",//IP
"cardNumber": "4564710000000004",//卡号 
"cardExpiry": "0219", //有效期
"cardName": "Bob Smith", //持卡人姓名
"cardCSC": "847", //CVV 后三位
//自定义字段
"customField1": "c1", 
"customField2": "c2", 
"customField3": "c3", 
"hashValue": "76312a970836073174a1e6dc3c252cc7"
```
|参数|描述|
|:---|:----|
|providerCode|提供商代码|
|providerMessage|提供商响应信息|
|providerResponseCode|提供商响应代码|
|sqidResponseCode|sqid响应代码|
|sqidResponseMessage|sqid响应信息|
|transactionID|业务ID|
|receiptNo|收据编号|
|custom1|自定义字段内容|
|custom2|自定义字段内容|
|custom3|自定义字段内容|
|hashValue|验证哈希,MD5(PassPhrase + ReceiptNo + APIKey) |
>"provider"参数显示来自上游提供商的响应。
>该"sqidResponseCode"有小于0的值表示有SRP验证错误。如果值为零，这笔交易是成功的和大于零的任何指示提供程序拒绝交易

```
{
    "providerCode": "724452380",
    "providerMessage": "Honour with identification",
    "providerResponseCode": "8",
    "sqidResponseCode": 0,
    "sqidResponseMessage": "Approved",
    "transactionID": "dea4cf6f-1cf7-4c0a-b68a-b454fd1474dc",
    "receiptNo": "21857",
    "custom1": "c1",
    "custom2": "c2",
    "custom3": "c3",
    "hashValue": "3c267b01071b9f7a5977da172eff83b1"
}
```
###getByReference
检索有关交易提供``referenceID``相匹配的信息。

|参数|描述|
|:---|:----|
|methodName|方法名|
|merchantCode|商户名称,这是SQID分配给你一个唯一的代码|
|apiKey|这是SQID分配给你一个唯一的代码|
|reference|交易最初时提供的字段（请确保它是唯一的，不然会返回多个结果）|
|hashValue|MD5(PassPhrase + reference + APIKey)有效长度：32个字符(全部小写)|

```
{
    "sqidResponseCode": 0,
    "sqidResponseMessage": "OK",
    "transactions": [
        {
            "clientCode": "EOBG",
            "receiptID": 21860,
            "transactionDate": "2015-07-27T18:37:03.02",
            "transactionAmount": 100,
            "refundDate": null,
            "refundAmount": null,
            "feeTotal": 1.87,
            "settlementAmount": 98.13,
            "referenceID": "54d3f33d624b44a78ae4f7157ef39eb4",
            "customField1": "",
            "customField2": "",
            "customField3": "",
            "customerName": "Bob Smith",
            "customerHouseStreet": "10 Ipswich Rd",
            "customerSuburb": "Annerley",
            "customerCity": "Brisbane",
            "customerState": "QLD",
            "customerCountry": "AUS",
            "customerPostCode": "4103",
            "customerMobile": "0400000000",
            "customerEmail": "person@company.com",
            "customerIP": "",
            "cardNumber": "456471....0004",
            "cardExpiry": "0219",
            "cardName": "Bob Smith",
            "currency": "AUD",
            "transactionStatus": 2
        }
    ]
}
```
``transactionStatus``表示订单状态

|参数|描述|
|:---|:----|
|2|Approved（已生效）|
|3|Declined（已拒绝）|
|8|Refunded（已退款）|
